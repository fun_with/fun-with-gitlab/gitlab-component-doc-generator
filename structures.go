package main

type Component struct {
	inputs []ComponentInput
}

type ComponentInput struct {
	name       string
	attributes []ComponentAttributes
}

type ComponentAttributes struct {
	description string `yaml:"description"`
}

type StringComponentAttributes struct {
	basicAttributes ComponentAttributes
	typeParam       string   `default:"string"`
	options         []string `yaml:"options"`
	regex           string   `default:""`
}

type BooleanComponentAttributes struct {
	basicAttributes ComponentAttributes
	typeParam       string `default:"boolean"`
	options         []bool `yaml:"options"`
}

type NumberComponentAttributes struct {
	basicAttributes ComponentAttributes
	typeParam       string `default:"number"`
	options         []int  `yaml:"options"`
}

const (
	STRING = "string"
	BOOL   = "boolean"
	NUMBER = "number"
)
