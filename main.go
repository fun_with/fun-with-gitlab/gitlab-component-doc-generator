package main

import (
	"fmt"
	"github.com/mitchellh/colorstring"
)

func main() {
	fmt.Println("Hello, World!")

	colorstring.Println("[blue]Hello [red]World!")

}
